package com.debugeverything;

import java.text.NumberFormat; // Helps with formatting doubles as currency
import java.util.Scanner; // Will be used to get input from the user

public class Main {

    public static void main(String[] args) {

        // Create and instantiate the Account object
        Account savings = new Account();
        savings.setBalance(0.00);

        NumberFormat formatter = NumberFormat.getCurrencyInstance(); // Creates the formatter object for currency
        Scanner sc = new Scanner(System.in); // Creates the sc object to read user input

        boolean session = true; // This variable will break the (while) loop when false

        while (session) {

            // Present the user with a menu of 4 options
            System.out.print("\nATM Menu: \n \n"
                    + "1. Deposit Money \n"
                    + "2. Withdraw Money \n"
                    + "3. Check Account Balance\n"
                    + "4. End Session\n \n"
                    + "Enter selection: ");

            int selection = sc.nextInt(); // assign the user's input to the selection variable
            // This switch block will handle one of five selections and deal with them appropriately
            switch (selection) {
                // case 1 handles the depositing of money
                case 1:
                    System.out.println("\nYour current Savings balance is: " + formatter.format(savings.getBalance()) + "\n");
                    System.out.println("How much money would you like to deposit?");
                    double deposit = 0;
                    do {
                        // check if is a valid double
                        if (sc.hasNextDouble()) {
                            deposit = sc.nextDouble();
                            // check if the user input is positive
                            if (deposit < 0) {
                                System.out.print("\nPlease, enter a valid amount: ");
                            } else {
                                savings.deposit(deposit);
                                break;
                            }
                        } else {
                            System.out.print("\nPlease, enter a valid amount: ");
                            sc.next();
                        }
                    } while (deposit <= 0);
                    System.out.println("\nYour Savings balance is now: " + formatter.format(savings.getBalance()) + "\n");
                    break;
                case 2:
                    System.out.println("\nYour current Savings balance is: " + formatter.format(savings.getBalance()) + "\n");
                    System.out.println("How much money would you like to withdraw?");
                    double withdraw = 0;
                    do {
                        // check if is a valid double
                        if (sc.hasNextDouble()) {
                            withdraw = sc.nextDouble();
                            // check if the user input is positive
                            if (withdraw < 0) {
                                System.out.print("\nPlease, enter a valid amount: ");
                            } else {
                                // check if the user has enough funds
                                if (withdraw > savings.getBalance()) {
                                    System.out.print("\nYou don't have enough funds. ");

                                } else {
                                    savings.withdraw(withdraw);
                                }
                                break;
                            }
                        } else {
                            System.out.print("\nPlease, enter a valid amount: ");
                            sc.next();
                        }
                    } while (withdraw <= 0);
                    System.out.println("\nYour Savings balance is now: " + formatter.format(savings.getBalance()) + "\n");
                    break;
                // case 3 handles the transfer of funds
                case 3:
                    System.out.println("Savings Balance: " + formatter.format(savings.getBalance()) + "\n");
                    break;
                // case 5 breaks out of the (while) loop when the user is finished using the ATM
                case 4:
                    session = false;
                    break;
            }
        }
        System.out.println("\nThank you for banking with us!\n");
    }
}


class Account {
    double balance;

    void setBalance(double accBal) {
        balance = accBal;
    }

    void deposit(double dep) {
        balance += dep; // Take the Account object's balance and add to it the current deposit
    }

    void withdraw(double wit) {
        balance -= wit; // Take the Account object's balance and subtract from it the current withdrawal
    }

    double getBalance() {
        return balance; // Take the current balance
    }
}

