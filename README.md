# Java ATM project


This program simulates a simple ATM environment.  
ATM supports deposits, withdrawals, and balance inquiries.  
@version 0.1

To run, download the main file, open it with your favorite IDE (IntelliJ Idea, Netbeans, Eclise...)
and run the program.
 
 
Created by Debug Everything  
[Email](mailto:admin@debugeverything.com)  - [Facebook](https://www.facebook.com/debugeverything/) - [Instagram](https://www.instagram.com/debugeverything/)  

